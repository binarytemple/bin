#!/usr/bin/zsh 

# This command tidies all archive files into a single sub-directory of your home directory.
# At an enormous runtime cost, this command could be upgraded to use heuristics such as the 
# file command, however, it is unlikely that one would wish to move jar files, etc as they 
# are often location dependent. It is however, an interesting idea.

# TODO: Investigate a Maven exporter that can generate a list of all files used in the build

setopt extendedglob

PAT=$(print ~/^zips/**/*.(zip|tgz|7z|gz)) > /dev/null 2>&1 \
  || ( echo "Exiting, no matches. No need to do any work" && exit )
TMPD=$(mktemp -d)
ZIPS="$(print ~/zips)"
test -d $ZIPS || mkdir -p $ZIPS 
eval "for i in $PAT; do mv \$i $ZIPS; done"
rmdir $TMPD
